import React from 'react';

export default class Notch extends React.Component {

  constructor() {
    super();
    this.state = {
      name: '',
      goal:'',
      category: 'used',
      dropdownOptions:[
        'learn',
        'read',
        'used',
        'published',
        'attended',
        'performed',
        'created'
      ]      
    };

     
  }

  componentDidMount() {
    this.props.onGetInvite();
  }

  render() {
    //console.log( this.props.notch)
    const { fullName, activities } = this.props.notch;
    return (
      <div  className="container">
        <h1>Skillnotch: {fullName}</h1>  


          
        {/* form */}
        <div className="form-inline">
          <p>What have you done lately to get better in your craft?</p>
          <select className="form-control" onChange={e => this.setState({ category: e.target.value })}>
          {this.state.dropdownOptions.map((option, index) => {
            return(
              <option key={index} value={option}>{option}</option>
              )
            }
          )}
          </select>   

        
          <input className="form-control" placeholder="Ex:Firebase" type="text" value={this.state.name} onChange={e => this.setState({ name: e.target.value })}/>
          <input size="80" className="form-control" placeholder="Ex:Experimentation with Firebase and React for personal project" type="text" value={this.state.goal} onChange={e => this.setState({ goal: e.target.value })}/>

          <button
            type="button"
            className="btn btn-primary"
            onClick={() => this.props.onAddActivity(this.state.name, this.state.category, this.state.goal)}
          >Save</button>

        </div>          
        
        {/* activities */}    
        <div >

          {activities && activities.length > 0 ? (
            <ul className="activities">
              {activities.map((activity, index) => {
                return (
                  <li className={"activity " + activity.category} key={index}>
                    <h3>{activity.name}</h3>
                    <p>{activity.goal}</p>
                    {activity.category}
                    
                    <span className="bottom">{activity.category}</span>              
                  </li>
                );
              })}
            </ul>
          ) : null}
        </div>
      
      </div>


                    
      

    );
  }
}
