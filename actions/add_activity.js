import ActionTypes from '../constants/action_types';
import database from './database';

export function addActivity(name, category, goal) {
  return dispatch => {
    dispatch(addActivityRequestedAction());
    const guestsRef = database.ref('/activities');
    guestsRef.push({
      name,
      category,
      goal
    })
    .then(() => {
      dispatch(addActivityFulfilledAction({ name, category, goal }));
    })
    .catch((error) => {
      dispatch(addActivityRejectedAction());
    });
  }
}


function addActivityRequestedAction() {
  return {
    type: ActionTypes.AddActivityRequested
  };
}

function addActivityRejectedAction() {
  return {
    type: ActionTypes.AddActivityRejected
  }
}

function addActivityFulfilledAction(name, category, goal) {
  return {
    type: ActionTypes.AddActivityFulfilled,
    name,
    category,
    goal
  };
}
