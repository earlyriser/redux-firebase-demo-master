import ActionTypes from '../constants/action_types';
import database from './database';

export function watchActivityAddedEvent(dispatch) {
  database.ref('/activities').on('child_added', (snap) => {
    dispatch(getActivityAddedAction(snap.val()));
  });
}

function getActivityAddedAction(activity) {
  return {
    type: ActionTypes.ActivityAdded,
    activity
  };
}
