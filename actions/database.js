import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyDab5ierXRqTB-0U1z5hw3QZYiYXzke9wY',
  authDomain: 'redux-firebase-8268c.firebaseapp.com',
  databaseURL: 'https://redux-firebase-8268c.firebaseio.com/'
};

firebase.initializeApp(config);
const database = firebase.database();

export default database;