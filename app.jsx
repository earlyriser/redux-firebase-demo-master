import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import NotchContainer from './containers/notch_container';
import store from './store/store';
import "./stylesheets/main.scss";

const main = (
  <Provider store={store}>
    <div>
      <NotchContainer />
    </div>
  </Provider>
);

ReactDOM.render(main, document.getElementById('container'));
