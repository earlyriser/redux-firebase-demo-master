import { connect } from 'react-redux';
import { getInvite } from '../actions/get_invite';
import { addActivity } from '../actions/add_activity';
import { watchActivityAddedEvent } from '../actions/activity_added_event';
import Notch from '../components/notch.jsx';

function mapStateToProps(state) {
  console.log(state)
  return {
    notch: state.notch
  };
}

function mapDispatchToProps(dispatch) {
  watchActivityAddedEvent(dispatch);
  return {
    onGetInvite: () => dispatch(getInvite()),
    onAddActivity: (name, category, goal) => dispatch(addActivity(name,category, goal))
  };
}

const notchContainer = connect(mapStateToProps, mapDispatchToProps)(Notch);

export default notchContainer;
