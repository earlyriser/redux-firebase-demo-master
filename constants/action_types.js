const actionTypes = {
  GetInviteRequested: 'GET_INVITE_REQUESTED',
  GetInviteRejected: 'GET_INVITE_REJECTED',
  GetInviteFulfilled: 'GET_INVITE_FULFILLED',

  AddToInviteRequested: 'ADD_TO_INVITE_REQUESTED',
  AddToInviteRejected: 'ADD_TO_INVITE_REJECTED',
  AddToInviteFulfilled: 'ADD_TO_INVITE_FULFILLED',
  
  AddActivityRequested: 'ADD_ACTIVITY_REQUESTED',
  AddActivityRejected: 'ADD_ACTIVITY_REJECTED',
  AddActivityFulfilled: 'ADD_ACTIVITY_FULFILLED',  

  ActivityAdded: 'ACTIVITY_ADDED',
  
  GuestAdded: 'GUEST_ADDED'
};

export default actionTypes;
