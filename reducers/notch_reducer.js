import ActionTypes from '../constants/action_types';

export function notchReducer(state = {}, action) {
  switch(action.type) {
    case ActionTypes.GetInviteRequested: {
      return Object.assign({}, state, {
        inProgress: true,
        error: '',
        success: ''
      });
    }
    case ActionTypes.GetInviteRejected: {
      return Object.assign({}, state, {
        inProgress: false,
        error: 'Error in getting invite.',
      });
    }
    case ActionTypes.GetInviteFulfilled: {
      const { host, agenda, guests, activities, fullName } = action.invite;
      const newState = Object.assign({}, state, {
        inProgress: false,
        success: 'Got invite.',
        host,
        agenda,
        fullName
      });

      newState.activities = [];
      if (activities) {
        newState.activities = Object.keys(activities).map(k => activities[k]);
        newState.activities = newState.activities.reverse();
      }      
       
      return newState;
    }


    case ActionTypes.AddActivityRequested: {
      return Object.assign({}, state, {
        inProgress: true,
        error: '',
        success: ''
      });
    }
    case ActionTypes.AddActivityRejected: {
      return Object.assign({}, state, {
        inProgress: false,
        error: 'Error in adding guest.',
      });
    }
    case ActionTypes.AddActivityFulfilled: {
      const newState = Object.assign({}, state, {
        inProgress: false,
        success: 'Added activity.'
      });
      return newState;
    }
    case ActionTypes.ActivityAdded: {
      const newState = Object.assign({}, state);
      newState.activities = newState.activities || [];
      newState.activities = newState.activities.slice();
      newState.activities.push(action.activity);
      return newState;
    }    

    default:
      return state;
  }
}
