import { combineReducers } from 'redux';
import { notchReducer } from './notch_reducer';

const rootReducer = combineReducers({
  notch: notchReducer
});

export default rootReducer;
